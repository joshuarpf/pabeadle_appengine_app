package main

import "pabeadle/pabeadle_appengine_app/app"

func main() {
	service := app.Gin()
	service.Run(":5000")
}