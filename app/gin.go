package app

import (
	"pabeadle/pabeadle_appengine_app/config"
	"pabeadle/pabeadle_appengine_app/middleware"

	"github.com/gin-gonic/gin"
)

func Gin() *gin.Engine {
	ginFramework := gin.Default()

	// firebase
	firebaseAuth := config.SetupFirebase()

	// Database
	db := config.CreateDatabase()

	ginFramework.Use(func(context *gin.Context) {
		context.Set("db", db)
		context.Set("firebaseAuth", firebaseAuth)
	})

	ginFramework.Use(middleware.AuthMiddleware)

	InitializeRoutes(ginFramework)

	return ginFramework
}
