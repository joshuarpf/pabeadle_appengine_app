package app

import (
	"pabeadle/pabeadle_appengine_app/handler"

	"github.com/gin-gonic/gin"
)

func InitializeRoutes(gin *gin.Engine) {
	// GET
	gin.GET("/artists", handler.FindArtists)

	// POST
	gin.POST("/artist", handler.CreateArtist)
}
